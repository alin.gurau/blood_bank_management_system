package ro.emanuel.project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ro.emanuel.project.helper.DBHelper;
import ro.emanuel.project.pojo.Receiver;
import ro.emanuel.project.pojo.SexType;

public class ReceiverDAO {

	// CREATE
	public static void createReceiver(Receiver r) throws SQLException {

		Connection conn = DBHelper.createConnection();

		String insertString = "INSERT INTO "
				+ "receivers(nume, prenume, dataNasterii, gen, cnp, adresa, email, numarTelefon, orderDate)"
				+ "values(?,?,?,?,?,?,?,?,?)";
		PreparedStatement stmt = conn.prepareStatement(insertString);
		stmt.setString(1, r.getNume());
		stmt.setString(2, r.getPrenume());
		stmt.setDate(3, new java.sql.Date(r.getDataNasterii().getTime()));
		int sexType = SexType.getIntValue(r.getGen());
		stmt.setInt(4, sexType);
		stmt.setString(5, r.getCnp());
		stmt.setString(6, r.getAdresa());
		stmt.setString(7, r.getEmail());
		stmt.setString(8, r.getNumarTelefon());
		stmt.setDate(9, new java.sql.Date(r.getOrderDate().getTime()));

		stmt.executeUpdate();

		DBHelper.closeConnection(conn);
	}

	// READ
	public static ArrayList<Receiver> getReceivers() throws SQLException {

		ArrayList<Receiver> result = new ArrayList<Receiver>();

		Connection conn = DBHelper.createConnection();

		String selectString = "select * from receivers";
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(selectString);

		while (rs.next()) {
			int id = rs.getInt("id");
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			java.util.Date dataNasterii = new java.util.Date(rs.getTimestamp("data_nasterii").getTime());
			int gen = rs.getInt("gen");
			SexType sType = SexType.getTypeByInt(gen);
			String cnp = rs.getString("cnp");
			String adresa = rs.getString("adresa");
			String email = rs.getString("email");
			String numarTelefon = rs.getString("numar_telefon");
			java.util.Date orderDate = new java.util.Date(rs.getTimestamp("order_date").getTime());

			Receiver r = new Receiver(id, nume, prenume, dataNasterii, sType, cnp, adresa, email, numarTelefon,
					orderDate);

			result.add(r);
		}
		DBHelper.closeConnection(conn);
		return result;
	}

	public static Receiver getReceiverById(int receiverId) throws SQLException {

		Receiver receiver = null;

		Connection conn = DBHelper.createConnection();

		String selectString = "select * from receivers where id=?";
		PreparedStatement stmt = conn.prepareStatement(selectString);
		stmt.setInt(1, receiverId);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			int id = rs.getInt("id");
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			java.util.Date dataNasterii = new java.util.Date(rs.getTimestamp("data_nasterii").getTime());
			int gen = rs.getInt("gen");
			SexType sType = SexType.getTypeByInt(gen);
			String cnp = rs.getString("cnp");
			String adresa = rs.getString("adresa");
			String email = rs.getString("email");
			String numarTelefon = rs.getString("numar_telefon");
			java.util.Date orderDate = new java.util.Date(rs.getTimestamp("order_date").getTime());

			receiver = new Receiver(id, nume, prenume, dataNasterii, sType, cnp, adresa, email, numarTelefon,
					orderDate);
		}
		DBHelper.closeConnection(conn);
		return receiver;
	}

	// UPDATE
	public static void updateReceiver(Receiver r) throws SQLException {

		Connection conn = DBHelper.createConnection();

		String updateString = "UPDATE receivers SET "
				+ "nume=?, prenume=?, data_nasterii=?, gen=?, cnp=?, adresa=?, email=?, numar_telefon=?, order_date=?"
				+ "where id=?";
		PreparedStatement stmt = conn.prepareStatement(updateString);
		stmt.setString(1, r.getNume());
		stmt.setString(2, r.getPrenume());
		stmt.setDate(3, (java.sql.Date) r.getDataNasterii());
		int sType = SexType.getIntValue(r.getGen());
		stmt.setInt(4, sType);
		stmt.setString(5, r.getCnp());
		stmt.setString(6, r.getAdresa());
		stmt.setString(7, r.getEmail());
		stmt.setString(8, r.getNumarTelefon());
		stmt.setDate(9, (java.sql.Date) r.getOrderDate());
		stmt.setInt(10, r.getId());

		stmt.executeUpdate();

		DBHelper.closeConnection(conn);
	}

	// DELETE
	public static void deleteReceiver(Receiver r) throws SQLException {

		Connection conn = DBHelper.createConnection();

		String deleteString = "DELETE from receivers where id=?";
		PreparedStatement stmt = conn.prepareStatement(deleteString);
		stmt.setInt(1, r.getId());

		stmt.executeUpdate();

		DBHelper.closeConnection(conn);
	}

	public static void deleteReceiveById(int id) throws SQLException {

		Connection conn = DBHelper.createConnection();

		String deleteString = "DELETE from receivers where id=?";
		PreparedStatement stmt = conn.prepareStatement(deleteString);
		stmt.setInt(1, id);

		stmt.executeUpdate();

		DBHelper.closeConnection(conn);
	}

}
